@extends('email.layout.master')
@section('content')
    <p>Hi {{ $user->name }},</p>
    <table border="1">
        <thead>
            <tr>
                <td>Name</td>
                <td>Code</td>
                <td>Description</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ $product->name }}</td>
                <td>{{ $product->code }}</td>
                <td>{{ $product->description }}</td>
            </tr>
        </tbody>
    </table>
    <p>Thank you.</p>
@endsection
