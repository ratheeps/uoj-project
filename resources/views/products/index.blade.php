@extends('layouts.app')
@section('title', 'Products List')
@section('page', 'Products')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('products.create') }}" class="btn btn-success pull-right">Create</a>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="content table-responsive table-full-width">
                    <table class="table table-hover table-striped">
                        <thead>
                        <tr>
                            <td>Id</td>
                            <td>Code</td>
                            <td>Name</td>
                            <td>Is Active</td>
                            <td>Actions</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $product)
                            <tr>
                                <td>{{ $product->id }}</td>
                                <td>{{ $product->code }}</td>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->is_active }}</td>
                                <td>
                                    <a href="{{ route('products.show', ['product' => $product->id]) }}" class="btn btn-success btn-xs">Show</a>
                                    <a href="{{ route('products.edit', ['product' => $product->id]) }}" class="btn btn-primary btn-xs">Edit</a>
                                    <a href="{{ route('products.delete', ['product' => $product->id]) }}" class="btn btn-danger btn-xs">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
