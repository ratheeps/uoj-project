@extends('layouts.app')
@section('title', 'Products List')
@section('page', 'Products')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3>Edit Product</h3>
            <div class="card">
                <div class="card-body pad">
                    {!! Form::open()->multipart()->route('products.update', ['product' => $product->id])->patch()->fill($product) !!}
                    @include('products._form')
                    {!! Form::submit("Update") !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
