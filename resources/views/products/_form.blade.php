{!! Form::file('images[]', 'Images')->attrs(['multiple' => true]) !!}
{!! Form::text('name', 'Name')->placeholder('enter the product name') !!}
{!! Form::textarea('description', 'Description') !!}
{!! Form::select('is_active', 'Is Active', ['Yes' => 'Yes', 'No' => 'No']) !!}
