@extends('layouts.app')
@section('title', 'Products List')
@section('page', 'Products')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3>{{ $product->name }}</h3>
        </div>
    </div>
    <div class="card pad">
        <div class="row">
            <div class="col-md-3">
                <b>Name :</b>
            </div>
            <div class="col-md-3">
                {{ $product->name }}
            </div>

            <div class="col-md-3">
                <b>Code:</b>
            </div>
            <div class="col-md-3">
                {{ $product->code }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <b>Description:</b>
            </div>
            <div class="col-md-6">
                {{ $product->description }}
            </div>
        </div>
    </div>
    <div>
        @foreach($images as $image)
            <img src="{{ asset('storage/products/' . $image->id . '.' . $image->extension) }}" alt="{{ $image->name }}" width="300px">
        @endforeach
    </div>
@endsection
