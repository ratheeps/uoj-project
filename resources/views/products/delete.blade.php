@extends('layouts.app')
@section('title', 'Products List')
@section('page', 'Products')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3>Delete Product</h3>
            <div class="card">
                <div class="card-body pad">
                    <div class="panel panel-danger">
                        <div class="panel-heading">Are you sure?</div>
                        <div class="panel-body">{{ $product->name }}</div>
                        {!!Form::open()->method('delete')->route('products.destroy', ['product' => $product->id])!!}
                        {!!Form::hidden('id')->value($product->id)!!}
                        {!!Form::submit('Delete')->danger()!!}
                        {!!Form::anchor("Cancel")->route('products.index')!!}
                        {!!Form::close()!!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
