@extends('layouts.app')
@section('title', 'Categories List')
@section('page', 'Categories')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('categories.create') }}" class="btn btn-success pull-right">Create</a>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="content table-responsive table-full-width">
                    <table class="table table-hover table-striped">
                        <thead>
                        <tr>
                            <td>Id</td>
                            <td>Name</td>
                            <td>Actions</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td>{{ $category->id }}</td>
                                <td>{{ $category->name }}</td>
                                <td>
{{--                                    <a href="{{ route('products.show', ['product' => $product->id]) }}" class="btn btn-success btn-xs">Show</a>--}}
                                    {{--<a href="{{ route('products.edit', ['product' => $product->id]) }}" class="btn btn-primary btn-xs">Edit</a>--}}
                                    {{--<a href="{{ route('products.delete', ['product' => $product->id]) }}" class="btn btn-danger btn-xs">Delete</a>--}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
