@extends('layouts.app')
@section('title', 'Products List')
@section('page', 'Products')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3>Create Category</h3>
            <div class="card">
                <div class="card-body pad">
                    {!! Form::open()->route('categories.store') !!}
                        @include('catrgories._form')
                    {!! Form::submit("Create") !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
