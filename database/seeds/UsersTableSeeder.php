<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = \Carbon\Carbon::now();
        $users = [
            [
                'name' => 'Raja',
                'email' => 'raafasv@sad.dsfd',
                'password' => Hash::make('123456'),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Vemal',
                'email' => 'vimal@sad.dsfd',
                'password' => Hash::make('123456'),
                'created_at' => $now,
                'updated_at' => $now
            ]
        ];

        \App\User::insert($users);
    }
}
