<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductStoreRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Mail\ProductCreatedNotification;
use App\Product;
use App\ProductImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        $products = Product::where('user_id', $user->id)->get();
        return view('products.index', compact('products'));
    }

    public function create(){
        return view('products.create');
    }

    public function edit(Product $product){
        return view('products.edit', compact('product'));
    }

    public function store(ProductStoreRequest $request)
    {
        $images = $request->file('images');

        $product = new Product();
        $product->setAttribute('code', uniqid());
        $product->setAttribute('name', $request->input('name'));
        $product->setAttribute('description', $request->input('description'));
        $product->setAttribute('is_active', $request->input('is_active'));
        $product->setAttribute('user_id', auth()->id());
        $product->save();

        $imagePath = 'public/products';
        foreach ($images as $image){
            $extension = $image->getClientOriginalExtension();
            $productImage = ProductImage::create([
                'name' => $image->getClientOriginalName(),
                'extension' => $extension,
                'product_id' => $product->id
            ]);
            $name = $productImage->id . '.' . $extension;
            $image->storeAs($imagePath, $name);
        }
        Mail::send(new ProductCreatedNotification($product, auth()->user()));
        return response()->redirectToRoute('products.index');
    }

    public function update(Product $product, ProductUpdateRequest $request)
    {
        $images = $request->file('images');

        $product->setAttribute('name', $request->input('name'));
        $product->setAttribute('description', $request->input('description'));
        $product->setAttribute('is_active', $request->input('is_active'));
        $product->save();

        $imagePath = 'public/products';
        if ($images){
            foreach ($images as $image){
                $extension = $image->getClientOriginalExtension();
                $productImage = ProductImage::create([
                    'name' => $image->getClientOriginalName(),
                    'extension' => $extension,
                    'product_id' => $product->id
                ]);
                $name = $productImage->id . '.' . $extension;
                $image->storeAs($imagePath, $name);
            }
        }

        return response()->redirectToRoute('products.index');
    }

    public function show(Product $product){
        $images = $product->images;
        return view('products.show', compact('product', 'images'));
    }

    public function delete(Product $product)
    {
        return view('products.delete', compact('product'));
    }

    public function destroy(Product $product, Request $request){
        $images = $product->images;
        foreach ($images as $image){
            $imagePath = 'public/products/';
            $name = $image->id . '.' . $image->extension;
            $imageFullPath = $imagePath . $name;
            if (Storage::exists($imageFullPath)){
                Storage::delete($imageFullPath);
            }
        }
        $product->images()->delete();
        $product->delete();
        return response()->redirectToRoute('products.index');
    }
}
