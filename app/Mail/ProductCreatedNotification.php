<?php

namespace App\Mail;

use App\Product;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProductCreatedNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $product;
    public $user;

    /**
     * Create a new message instance.
     *
     * @param Product $product
     * @param User $user
     */
    public function __construct(Product $product, User $user)
    {
        $this->product = $product;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->to($this->user->email);
        $this->subject('Product Create Notification');
        $imagePath = 'public/products/';
        foreach ($this->product->images as $image){
            $name = $image->id . '.' . $image->extension;
            $this->attachFromStorage($imagePath . $name, $image->name);
        }
        return $this->view('email.product-created');
    }
}
