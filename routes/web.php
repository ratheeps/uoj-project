<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');


Route::middleware(['auth'])->group(function () {

    Route::prefix('products')->group(function () {
        Route::get('/', 'ProductController@index')->name('products.index');
        Route::get('/create', 'ProductController@create')->name('products.create');
        Route::post('/', 'ProductController@store')->name('products.store');
        Route::get('/{product}', 'ProductController@show')->name('products.show');

        Route::get('/edit/{product}', 'ProductController@edit')->name('products.edit');
        Route::patch('update/{product}', 'ProductController@update')->name('products.update');

        Route::get('/delete/{product}', 'ProductController@delete')->name('products.delete');
        Route::delete('destroy/{product}', 'ProductController@destroy')->name('products.destroy');
    });

    Route::prefix('categories')->group(function ($route){
        $route->get('/', 'CategoryController@index')->name('categories.index');
        $route->get('/create', 'CategoryController@create')->name('categories.create');
        $route->post('/', 'CategoryController@store')->name('categories.store');
    });
});
